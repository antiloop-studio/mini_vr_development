﻿using UnityEngine;
using UnityEngine.UI;


public class TextFPSCounter : MonoBehaviour 
{
	public Text text;
	public bool show;

	private const int targetFPS = 
		#if UNITY_ANDROID
		60;
		#else
		75;
		#endif
	private const float updateInterval = 0.5f;

	private int framesCount; 
	private float framesTime; 

	
	void Update()
	{
		framesCount++;
		framesTime += Time.unscaledDeltaTime; 
        
		if (framesTime > updateInterval)
		{
			if (text != null)
			{
				if (show)
				{
					float fps = framesCount/framesTime;
					text.text = System.String.Format("{0:F2} FPS", fps);
					text.color = (fps > (targetFPS-5) ? Color.green :
					             (fps > (targetFPS-30) ?  Color.yellow : 
					              Color.red));
				}
				else
				{
					text.text = "";
				}
			}

			framesCount = 0;
			framesTime = 0;
		}
		
	}
}
