﻿Shader "InflightVR/BlendTexture" { 
 
	Properties {
		_Blend ("Blend", Range (0, 1) ) = 0.5 
		_MainTex ("Texture 1", 2D) = "white" {} 
		_Texture2 ("Texture 2", 2D) = "white" {}
		_TransparencyAmount ("TransparencyAmount", Range (0, 1) ) = 0.5 
	}
 
	SubShader {	
		Tags {"Queue" = "Transparent"}
		ZWrite off
        CGPROGRAM
        #pragma surface surf NoLighting alpha:fade

        sampler2D _MainTex;
        sampler2D _Texture2;
        fixed _TransparencyAmount;
		fixed _Blend;
    
        struct Input {
            float2 uv_MainTex;
        };

        void surf (Input IN, inout SurfaceOutput o) {
			float4 tex1 = tex2D(_MainTex, IN.uv_MainTex);
			float4 tex2 = tex2D(_Texture2, IN.uv_MainTex);
			o.Albedo.rgb = lerp(tex1, tex2, _Blend);
			o.Alpha = _TransparencyAmount;
        }

		fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
		{
			fixed4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
		}	

        ENDCG
	} 
 FallBack "Diffuse"
}