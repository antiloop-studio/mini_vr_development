﻿Shader "Inflight/Glow" {
	Properties {
		_MainTex("Base (RGB)", 2D) = "white" {}
		_CutoutTex("Cutout (RGB)", 2D) = "white" {}
		_GlowColor ("Glow Color", Color ) = ( 1.0, 1.0, 1.0, 1.0 )
		_Frequency( "Glow Frequency", Float ) = 1.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D	_MainTex;
		sampler2D	_CutoutTex;
		fixed4		_GlowColor;
		half		_Frequency;

		struct Input {
			float2 uv_MainTex;
			float2 uv_CutoutTex;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half _MinPulseVal = sin(_Time.y * _Frequency);
			half4 c = tex2D(_CutoutTex, IN.uv_CutoutTex);
			half4 base = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = base.rgb;
			o.Emission = c.rgb * _GlowColor.rgb * _MinPulseVal;
			o.Alpha = base.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}