﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManager : MonoBehaviour
{
    public static AppStates appState;
    private IEnumerator CurrentCoRoutine;
    public static event Action<AppStates> appStateChange;

    void Start()
    {
        VideosController.videoDataLoaded += StartApp;
        GazeController.staticUiOptionSelected += OnUiOptionSelected;
    }

    void StartApp()
    {
        ChangeAppState(AppStates.InPlaneStart);
        StartNewCoRoutine(PublicAppData.StartInPlaneLoopTimestamp / 1000, AppStates.InPlaneLoop);
    }

    void OnUiOptionSelected(UiIds id)
    {

        if(CurrentCoRoutine != null) CancelCurrentCoRoutine();


        if( id == UiIds.InPlaneOption1)
        {
            ChangeAppState(AppStates.ReturnInplaneStart);
            StartNewCoRoutine((PublicAppData.StartInPlaneLoopTimestamp - PublicAppData.ReturnToInPlaneStartTimestamp) / 1000, AppStates.InPlaneLoop);
        }
        else if(id == UiIds.InPlaneOption2)
        {
            ChangeAppState(AppStates.Falling);
        }
        else if( id == UiIds.FallingOption1)
        {
            ChangeAppState(AppStates.ReturnInplaneStart);
            StartNewCoRoutine((PublicAppData.StartInPlaneLoopTimestamp - PublicAppData.ReturnToInPlaneStartTimestamp) / 1000, AppStates.InPlaneLoop);
        }
        else if( id == UiIds.FallingOption2)
        {
            ChangeAppState(AppStates.OutsideCar360);
        }
        else if( id == UiIds.Outside360CarOption3)
        {
            ChangeAppState(AppStates.InsideCar360);
        }
        else if( id == UiIds.Inside360CarOption1)
        {
            ChangeAppState(AppStates.OutsideCar360);
        }
        else if( id == UiIds.ReturnToPlaneOption1)
        {
            ChangeAppState(AppStates.ReturnInplaneStart);
            StartNewCoRoutine((PublicAppData.StartInPlaneLoopTimestamp - PublicAppData.ReturnToInPlaneStartTimestamp) / 1000, AppStates.InPlaneLoop);
        }
    }

    public void ChangeAppState(AppStates state)
    {
        if(state != appState)
        {
            appState = state;
            if(appStateChange != null) appStateChange(appState);
        }
    }

    void StartNewCoRoutine(float delay, AppStates state)
    {
        CurrentCoRoutine = DelayBeforeNextState(5, AppStates.InPlaneLoop);
        StartCoroutine(CurrentCoRoutine);
    }

    void CancelCurrentCoRoutine()
    {
        StopCoroutine(CurrentCoRoutine);
    }

    IEnumerator DelayBeforeNextState(float delay, AppStates newstate)
    {
        yield return new WaitForSeconds(delay);
        ChangeAppState(newstate);
    }

    public AppStates GetCurrentAppState()
    {
        return appState;
    }
}

public enum AppStates
{
    StartUp,
    InPlaneStart,
    ReturnInplaneStart,
    InPlaneLoop,
    Falling,
    OutsideCar360,
    InsideCar360,
    ExperienceEnded
}
