﻿using UnityEngine;

public class HotspotInfo : MonoBehaviour
{
    // public Vector2 hotspotLocation2D;
    public Vector3 hotspotLocation3D;
    [Header("Only add if hotspot has no information UI child")]
    public Texture newImage;

    private bool hotspotHasInformation = false;
    private GameObject informationGameObject;
    private GameObject openHotspotIndicator;
    private GameObject closeHotspotIndicator;
    private VideosController imagesController;

    void Start()
    {
        imagesController = FindObjectOfType<VideosController>();

        openHotspotIndicator = this.transform.Find("OpenHotspotIndicator").gameObject;
        var infoObj = this.transform.Find("HotspotInformationElement");
        if(infoObj != null) 
        {
            informationGameObject = infoObj.gameObject;
            hotspotHasInformation = true;
        }
        var closeObj = this.transform.Find("CloseHotspotIndicator");
        if(closeObj != null) closeHotspotIndicator = closeObj.gameObject;
        else 
        {
            var infoCloseObj = this.transform.Find("CloseHotspotInformationIndicator");
            if(infoCloseObj != null) closeHotspotIndicator = infoCloseObj.gameObject;
        }
    }

    void Update()
    {
        this.transform.rotation = Quaternion.LookRotation(this.transform.position);
    }

    public void OpenHotspot()
    {
        HotspotInteraction(true);
    }

    public void CloseHotspot()
    {
        HotspotInteraction(false);
    }

    void HotspotInteraction(bool open)
    {
        if(open)
        {
            if(hotspotHasInformation)
            {
                openHotspotIndicator.SetActive(false);
                informationGameObject.SetActive(true);
                closeHotspotIndicator.SetActive(true);
            }
            else
            {
                openHotspotIndicator.SetActive(false);
                closeHotspotIndicator.SetActive(true);
                imagesController.SetImageSphereImage(newImage);
                imagesController.DisplayImageOrVideo(false);
            }
        }
        else
        {
            if(hotspotHasInformation)
            {
                closeHotspotIndicator.SetActive(false);
                openHotspotIndicator.SetActive(true);
                informationGameObject.SetActive(false);
            }
            else
            {
                openHotspotIndicator.SetActive(true);
                closeHotspotIndicator.SetActive(false);
                imagesController.DisplayImageOrVideo(true);
            }
        }
    }
}
