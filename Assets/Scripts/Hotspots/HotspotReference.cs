﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotspotReference : MonoBehaviour
{
    public int openOrClose;
    [HideInInspector]
    public HotspotInfo hotspotReference;

    void Start()
    {
        hotspotReference = this.gameObject.GetComponentInParent<HotspotInfo>();
    }
}
