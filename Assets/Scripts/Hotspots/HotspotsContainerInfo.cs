﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotspotsContainerInfo : MonoBehaviour
{
    public Vector2 imageDimensions;
    public float indicatorInset;

    private HotspotInfo[] hotspots;
    private float offsetRadius;

    void Start()
    {
        offsetRadius = 5 - indicatorInset;
        hotspots = new HotspotInfo[this.transform.childCount];
        for (int i = 0; i < this.transform.childCount; i++)
        {
            var htspt = this.transform.GetChild(i).GetComponent<HotspotInfo>();
            // TEMP: random location in the sphere:
            htspt.hotspotLocation3D = Random.onUnitSphere * 4;
            
            htspt.transform.position = htspt.hotspotLocation3D; // FIXME: PlaceMenu(htspt.hotspotLocation2D);
            hotspots[i] = htspt;
        }
    }

    Vector3 PlaceMenu(Vector2 loc2d)
    {
        var phi = 2 * Mathf.PI * (loc2d.x / imageDimensions.x); // /0
        var theta = ( loc2d.y / imageDimensions.y) * Mathf.PI; // 0

        var pos = new Vector3(Mathf.Sin(phi) * Mathf.Cos(theta), Mathf.Sin(phi) * Mathf.Sin(theta), Mathf.Cos(phi));
        pos *= offsetRadius;

        return pos;
    }
}
