﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotspotsController : MonoBehaviour
{
    public GameObject insideHotSpots;
    public List<GameObject> hotspotContainers;

    private List<HotspotReference> openHotspots;
    private List<IEnumerator> runningCoRoutines = new List<IEnumerator>();

    void Start()
    {
        AppManager.appStateChange += OnAppStateChanged;
        GazeController.hotspotOptionSelected += OnHotspotSelected;
        PlaybackController.carRotated += OnCarRotatedEvent;
        PlaybackController.carStartRotating += OnCarStartRotating;

        openHotspots = new List<HotspotReference>();
    }

    void OnHotspotSelected(HotspotReference reference)
    {
        if(reference.openOrClose == 1) 
        {
            closeAllOpenHotspots();
            reference.hotspotReference.OpenHotspot();
            openHotspots.Add(reference);
        }
        else 
        {
            reference.hotspotReference.CloseHotspot();
        }
    }

    void OnAppStateChanged(AppStates state)
    {
        closeAllOpenHotspots();
        HideAllHotspots();

        if(state == AppStates.InsideCar360)
        {
            StartNewCoRoutine(1.5f, DisplayHotspotsGroup, 100, true);
        }
    }

    void OnCarStartRotating()
    {
        closeAllOpenHotspots();
        HideAllHotspots();
    }

    void OnCarRotatedEvent(int index)
    {
        if(AppManager.appState == AppStates.OutsideCar360)
        {
            if(index > hotspotContainers.Count - 1) return;
            HideAllHotspots();
            StartNewCoRoutine(1.5f, DisplayHotspotsGroup, index, true);
        }
    }

    void closeAllOpenHotspots()
    {
        foreach (var hotspotReference in openHotspots)
        {
            hotspotReference.hotspotReference.CloseHotspot();
        }
        openHotspots.Clear();
    }

    void HideAllHotspots()
    {
        for (int i = 0; i < hotspotContainers.Count; i++)
        {
            DisplayHotspotsGroup(i, false);
        }
    }

    void DisplayHotspotsGroup(int containerIndex, bool display)
    {
        if(containerIndex == 100) insideHotSpots.SetActive(display);
        else hotspotContainers[containerIndex].SetActive(display);
        
    }

    void StartNewCoRoutine<T, Y>(float delay, Action<T, Y> fadeaction, T param, Y param2)
    {
        var newcoroutine = DelayBeforeVideoAction(delay, fadeaction, param, param2);
        runningCoRoutines.Add(newcoroutine);
        StartCoroutine(newcoroutine);
    }

    IEnumerator DelayBeforeVideoAction<T, Y>(float delay, Action<T, Y> fadeaction, T param, Y param2)
    {
        yield return new WaitForSeconds(delay);
        fadeaction(param, param2);
    }

    void CancelAllCoRoutines()
    {
        foreach (var coroutine in runningCoRoutines)
        {
            StopCoroutine(coroutine);
        }
    }
}
