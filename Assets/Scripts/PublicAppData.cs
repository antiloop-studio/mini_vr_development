﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PublicAppData : MonoBehaviour
{
    public static float FadeSpeed = 1.2f;
    public static float FadeAppInOut = 2f;
    public static float ShowUiDelay = 1.2f;
    public static float SetVideoDelay = 1.2f;
    public static float ReturnToInPlaneStartTimestamp = 0;
    public static float StartInPlaneLoopTimestamp = 4000;
    // here could come more but depends on requirements;
}
