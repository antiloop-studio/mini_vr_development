﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FadeController : MonoBehaviour
{
    public Image fadeScreen; 
    private List<IEnumerator> runningCoRoutines = new List<IEnumerator>();

    void Start()
    {
        AppManager.appStateChange += OnAppStateChange;
    }

    void OnAppStateChange(AppStates state)
    {
        if(state == AppStates.InPlaneStart)
        {
            FadeToTransparent(PublicAppData.FadeAppInOut);
        }
        else if(state == AppStates.InPlaneLoop)
        {
            CancelAllCoRoutines();
        }
        else if(state == AppStates.OutsideCar360)
        {
            CancelAllCoRoutines();
            FadeToBlack(PublicAppData.FadeSpeed);
            StartNewCoRoutine(PublicAppData.FadeSpeed, FadeToTransparent, PublicAppData.FadeSpeed);
        }
        else if(state == AppStates.InsideCar360)
        {
            CancelAllCoRoutines();
            FadeToBlack(PublicAppData.FadeSpeed);
            StartNewCoRoutine(PublicAppData.FadeSpeed, FadeToTransparent, PublicAppData.FadeSpeed);
        }
        else if(state == AppStates.ExperienceEnded)
        {
            CancelAllCoRoutines();
            FadeToBlack(PublicAppData.FadeAppInOut);
        }
    }

    void StartNewCoRoutine(float delay, Action<float> fadeaction, float fadeSpeed)
    {
        var newcoroutine = DelayBeforeNewFade(delay, fadeaction, fadeSpeed);
        runningCoRoutines.Add(newcoroutine);
        StartCoroutine(newcoroutine);
    }

    IEnumerator DelayBeforeNewFade(float delay, Action<float> fadeaction, float fadeSpeed)
    {
        yield return new WaitForSeconds(delay);
        fadeaction(fadeSpeed);
    }

    void CancelAllCoRoutines()
    {
        foreach (var coroutine in runningCoRoutines)
        {
            StopCoroutine(coroutine);
        }
    }

    void FadeToBlack(float duration)
    {
        fadeScreen.CrossFadeAlpha(1, duration, false);
    }

    void FadeToTransparent(float duration)
    {
        fadeScreen.CrossFadeAlpha(0, duration, false);
    }
}
