﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GazeController : MonoBehaviour
{
    public float selectConfirmSpeed, reduceSpeedMultiplier;
    public GameObject gazeContainer;
    public Image loadingIndicator;
    public Camera viewCamera;

    public static event Action<UiIds> staticUiOptionSelected;
    public static event Action<HotspotReference> hotspotOptionSelected;

    private float gazeDistFromCamera = 4.8f;
    private float progress;
    private bool gazeDisabled = false, gazeVisible = true, uiOptionSelected = false;
    private int InteractMask;

    void Start()
    {
        AppManager.appStateChange += OnAppStateChange;
        InteractMask = LayerMask.NameToLayer("InteractUI");
    }

    void OnAppStateChange(AppStates state)
    {
        if(state == AppStates.InPlaneLoop)
        {
            DisplayGaze(true);
        }
    }

    void Update()
    {

        if(!gazeDisabled && gazeVisible)
        {
            RaycastHit HitInfo;
            if (Physics.Raycast(viewCamera.transform.position, viewCamera.transform.forward, out HitInfo, 5.2f))
            {
                gazeDistFromCamera = Vector3.Distance(HitInfo.point, viewCamera.transform.position) - .2f;

                if(HitInfo.collider.gameObject.layer == InteractMask)
                {
                    if (!uiOptionSelected) 
                    {
                        uiOptionSelected = true;
                        progress = 0;
                    }
                    if (uiOptionSelected)
                    {
                        if (progress < 1)
                        {
                            progress += Time.deltaTime / selectConfirmSpeed;
                            loadingIndicator.fillAmount = Mathf.Lerp(0, 1, progress);
                        }

                        if (progress >= 1)
                        {
                            progress = 0;
                            loadingIndicator.fillAmount = progress;
                            uiOptionSelected = false;
                            UiIdentifierData staticUiIdentfier = HitInfo.collider.GetComponent<UiIdentifierData>();
                            HotspotReference hotspotUi = HitInfo.collider.GetComponent<HotspotReference>();
                            if(staticUiOptionSelected != null && staticUiIdentfier != null) staticUiOptionSelected(staticUiIdentfier.id);
                            if(hotspotOptionSelected != null && hotspotUi != null) hotspotOptionSelected(hotspotUi);
                        }
                    }
                }
            }
            else
            {
                if (uiOptionSelected)
                { 
                    uiOptionSelected = false;
                    if(progress >= 1 || progress <= 0)
                    {
                        progress = 0;
                        loadingIndicator.fillAmount = 0;
                    }
                    else
                    {
                        progress = (1 - progress);
                    }
                    
                }
                if(!uiOptionSelected && (progress < 1 && progress > 0))
                {
                    if(progress < 1)
                    {
                        progress += Time.deltaTime / (selectConfirmSpeed / reduceSpeedMultiplier);
                        loadingIndicator.fillAmount = Mathf.Lerp(1, 0, progress);
                    }

                    if (progress >= 1)
                    {
                        progress = 0;
                    }
                }

                if (gazeDistFromCamera != 4.8f)
                {
                    gazeDistFromCamera = 4.8f;
                }
            }
        }

        gazeContainer.transform.position = viewCamera.transform.forward * gazeDistFromCamera;
        gazeContainer.transform.LookAt(viewCamera.transform);


        // TEMP: KEYBOARD CONTROLS FOR THE EDITOR:
        if(Input.GetKeyUp(KeyCode.Q)) if(staticUiOptionSelected != null) staticUiOptionSelected(UiIds.InPlaneOption1);
        if(Input.GetKeyUp(KeyCode.W)) if(staticUiOptionSelected != null) staticUiOptionSelected(UiIds.InPlaneOption2);
        if(Input.GetKeyUp(KeyCode.E)) if(staticUiOptionSelected != null) staticUiOptionSelected(UiIds.FallingOption1);
        if(Input.GetKeyUp(KeyCode.R)) if(staticUiOptionSelected != null) staticUiOptionSelected(UiIds.FallingOption2);
        if(Input.GetKeyUp(KeyCode.T)) if(staticUiOptionSelected != null) staticUiOptionSelected(UiIds.Outside360CarOption1);
        if(Input.GetKeyUp(KeyCode.Y)) if(staticUiOptionSelected != null) staticUiOptionSelected(UiIds.Outside360CarOption2);
        if(Input.GetKeyUp(KeyCode.U)) if(staticUiOptionSelected != null) staticUiOptionSelected(UiIds.Outside360CarOption3);
        if(Input.GetKeyUp(KeyCode.I)) if(staticUiOptionSelected != null) staticUiOptionSelected(UiIds.Inside360CarOption1);
        if(Input.GetKeyUp(KeyCode.O)) if(staticUiOptionSelected != null) staticUiOptionSelected(UiIds.ReturnToPlaneOption1);
    }

    public void GazeDisabled(bool disabled)
    {
        if(!gazeDisabled && disabled) 
        {
            gazeDisabled = disabled;
            gazeContainer.GetComponent<CanvasGroup>().alpha = 0.5f;
        }
        else if(gazeDisabled && !disabled)
        {
            gazeDisabled = disabled;
            gazeContainer.GetComponent<CanvasGroup>().alpha = 1;
        }
    }

    void DisplayGaze(bool display)
    {
        gazeVisible = display;
        gazeContainer.SetActive(display);
        GazeDisabled(!display);
    }
}
