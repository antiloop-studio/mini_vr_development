﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticUiController : MonoBehaviour
{
    public List<UiContainer> uiContainers;
    public Camera playerView;

    private List<IEnumerator> runningCoRoutines = new List<IEnumerator>();

    void Start()
    {
        AppManager.appStateChange += OnAppStateChanged;
    }

    void OnAppStateChanged(AppStates state)
    {
        HideAllUi();

        if(state == AppStates.InPlaneLoop)
        {
            ShowUi(StaticUiId.InplaneUi);
        }
        else if(state == AppStates.Falling)
        {
            StartNewCoRoutine(PublicAppData.StartInPlaneLoopTimestamp / 1000, ShowUi, StaticUiId.FallingUi);
        }
        else if(state == AppStates.OutsideCar360)
        {
            StartNewCoRoutine(PublicAppData.ShowUiDelay, ShowUi, StaticUiId.Car360OutsideUi);
            StartNewCoRoutine(PublicAppData.ShowUiDelay, ShowUi, StaticUiId.ReturnToPlane);
        }
        else if(state == AppStates.InsideCar360)
        {
            StartNewCoRoutine(PublicAppData.ShowUiDelay, ShowUi, StaticUiId.Car360InsideUi);
            StartNewCoRoutine(PublicAppData.ShowUiDelay, ShowUi, StaticUiId.ReturnToPlane);
        }
    }

    void StartNewCoRoutine<T>(float delay, Action<T> uiAction, T param)
    {
        var newcoroutine = playbackActionDelay(delay, uiAction, param);
        runningCoRoutines.Add(newcoroutine);
        StartCoroutine(newcoroutine);
    }

    IEnumerator playbackActionDelay<T>(float delay, Action<T> uiAction, T param)
    {
        yield return new WaitForSeconds(delay);
        uiAction(param);
    }

    void CancelAllCoRoutines()
    {
        foreach (var coroutine in runningCoRoutines)
        {
            StopCoroutine(coroutine);
        }
    }

    void ShowUi(StaticUiId uiGroup)
    {
        DisplayUi(true, uiGroup);
    }

    void HideUi(StaticUiId uiGroup)
    {
        DisplayUi(false, uiGroup);
    }

    void HideAllUi()
    {
        for (int i = 0; i < uiContainers.Count; i++)
        {
            HideUi((StaticUiId)i);
        }
    }

    void DisplayUi(bool display, StaticUiId uiGroup)
    {
        for (int i = 0; i < uiContainers[(int)uiGroup].UiOptions.Count; i++)
        {
            var option = uiContainers[(int)uiGroup].UiOptions[i];
            option.SetActive(display);
        }
    }

    [System.Serializable]
    public class UiContainer
    {
        public List<GameObject> UiOptions;
    }

    public enum StaticUiId
    {
        InplaneUi = 0,
        FallingUi = 1,
        Car360OutsideUi = 2,
        Car360InsideUi = 3,
        ReturnToPlane = 4
    }
}
