﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiIdentifierData : MonoBehaviour
{
    public UiIds id;

    void Update()
    {
        this.transform.rotation = Quaternion.LookRotation( this.transform.position);   
    }
}


public enum UiIds
{
    InPlaneOption1,
    InPlaneOption2,
    FallingOption1,
    FallingOption2,
    Outside360CarOption1,
    Outside360CarOption2,
    Outside360CarOption3,
    Inside360CarOption1,
    ReturnToPlaneOption1
}