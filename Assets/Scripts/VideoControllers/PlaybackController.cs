﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;

public class PlaybackController : MonoBehaviour
{
    [Range(0, 4)]
    public float moveSpeed;
    public int videoDivisions;

    public static event Action carStartRotating;
    public static event Action<int> carRotated;

    private IMediaControl videoControls;
    private IMediaInfo videoInfo;
    private List<IEnumerator> runningCoRoutines = new List<IEnumerator>();
    private bool start360InteractiveCar = false, videoIsPlaying = false, videoPlayedForward, videoDataLoaded;
    private float[] videoTimeStamps;
    private int currentTimestampIndex = 0;
    private int hotSpotsIndex = 0;
    private float offset;

    void Start()
    {
        AppManager.appStateChange += OnAppStateChanged;
        VideosController.videoChanged += OnVideoChanged;
        GazeController.staticUiOptionSelected += OnUiOptionSelected;
    }

    void OnUiOptionSelected(UiIds id)
    {
        if(id== UiIds.InPlaneOption1)
        {
            JumpToTimeStamp(PublicAppData.ReturnToInPlaneStartTimestamp);
        }
        else if(id == UiIds.Outside360CarOption1)
        {
            if(!videoIsPlaying) 
            {
                if(carStartRotating != null) carStartRotating();
                PlayToNextTimestamp(true);
            }
        }
        else if(id== UiIds.Outside360CarOption2)
        {
            if(!videoIsPlaying) 
            {
                if(carStartRotating != null) carStartRotating();
                PlayToNextTimestamp(false);
            }
        }
    }

    void OnAppStateChanged(AppStates state)
    {
        if(state == AppStates.InPlaneStart)
        {
            StartNewCoRoutine(.5f, PlayVideo);
        }
        else if(state == AppStates.ReturnInplaneStart)
        {
            StartNewCoRoutine(0, JumpToTimeStamp, PublicAppData.ReturnToInPlaneStartTimestamp);
        }
        else if(state == AppStates.Falling)
        {
            StartNewCoRoutine(.1f, PlayVideo);
        }
        else if(state == AppStates.OutsideCar360)
        {
            if(carRotated != null) carRotated(hotSpotsIndex);
            start360InteractiveCar = true;
        }
    }

    void OnVideoChanged(MediaPlayer player)
    {
        videoControls = player.Control;
        videoInfo = player.Info;
        videoDataLoaded = true;
        videoTimeStamps = GetVideoDivisionTimestamps(videoDivisions, videoInfo.GetDurationMs());
    }

    void StartNewCoRoutine<T>(float delay, Action<T> playbackAction, T param = default(T))
    {
        var newcoroutine = playbackActionDelay(delay, playbackAction, param);
        runningCoRoutines.Add(newcoroutine);
        StartCoroutine(newcoroutine);
    }

    IEnumerator playbackActionDelay<T>(float delay, Action<T> playbackAction, T param = default(T))
    {
        yield return new WaitForSeconds(delay);
        playbackAction(param);
    }

    void StartNewCoRoutine(float delay, Action playbackAction)
    {
        var newcoroutine = playbackActionDelay(delay, playbackAction);
        runningCoRoutines.Add(newcoroutine);
        StartCoroutine(newcoroutine);
    }

    IEnumerator playbackActionDelay(float delay, Action playbackAction)
    {
        yield return new WaitForSeconds(delay);
        playbackAction();
    }

    void CancelAllCoRoutines()
    {
        foreach (var coroutine in runningCoRoutines)
        {
            StopCoroutine(coroutine);
        }
    }

    void JumpToTimeStamp(float timestamp)
    {
        videoControls.SeekFast(timestamp);
    }

    void PlayVideo()
    {
        videoControls.Play();
    }

    void PauseVideo()
    {
        videoControls.Pause();
    }

    void Update()
    {
        if(videoDataLoaded)
        {
            if(start360InteractiveCar)
            {
                if (currentTimestampIndex + 1 > videoTimeStamps.Length - 1)
                {
                    if (videoControls.GetCurrentTimeMs() >= videoInfo.GetDurationMs() && videoIsPlaying)
                    {
                        NextTimestampReached(true);
                    }
                }
                else
                {
                    if (videoControls.GetCurrentTimeMs() >= (videoTimeStamps[currentTimestampIndex + 1] + offset) && videoIsPlaying)
                    {
                        NextTimestampReached(false);
                    }
                }
            }

            if(AppManager.appState == AppStates.InPlaneLoop)
            {
                if(videoControls.GetCurrentTimeMs() >= videoInfo.GetDurationMs())
                {
                    JumpToTimeStamp(PublicAppData.StartInPlaneLoopTimestamp);
                }
            }
        }
    }

    void PlayToNextTimestamp(bool forward)
    {
        var index = currentTimestampIndex;

        if(forward)
        {
            hotSpotsIndex++;
            if(hotSpotsIndex >= videoDivisions) hotSpotsIndex = 0;

            if(index >= videoDivisions) 
            {
                currentTimestampIndex = videoTimeStamps.Length - index;
                if(currentTimestampIndex == videoDivisions) currentTimestampIndex = 0;
                videoControls.SeekFast(videoTimeStamps[currentTimestampIndex]);
            }

            videoPlayedForward = true;
        }
        else
        {
            hotSpotsIndex--;
            if(hotSpotsIndex < 0) hotSpotsIndex = 4;

            if(index <= videoDivisions - 1) 
            {
                currentTimestampIndex = videoTimeStamps.Length - index;
                if(currentTimestampIndex == videoTimeStamps.Length) currentTimestampIndex = videoDivisions;
                videoControls.SeekFast(videoTimeStamps[currentTimestampIndex]);
            }

            videoPlayedForward = false;
        }

        if(hotSpotsIndex >= videoDivisions) hotSpotsIndex -= videoDivisions;

        PlayVideo();
        videoIsPlaying = true;
    }

    void NextTimestampReached(bool videoEnded)
    {
        PauseVideo();
        videoIsPlaying = false;

        if(videoEnded)
        {
            currentTimestampIndex = videoDivisions;
            videoControls.SeekFast(videoTimeStamps[currentTimestampIndex]);
        }
        else
        {
            currentTimestampIndex++;
        }

        if(carRotated != null) carRotated(hotSpotsIndex);
    }

    float[] GetVideoDivisionTimestamps(int divisions, float videoLength)
    {
        float[] vidtimestamps = new float[divisions * 2];
        float timechunk = videoLength / vidtimestamps.Length;

        for (int i = 0; i < vidtimestamps.Length; i++)
        {
            vidtimestamps[i] = i * timechunk;
        }

        return vidtimestamps;
    }
}
