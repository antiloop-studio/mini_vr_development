﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;

public class VideosController : MonoBehaviour
{
    public Texture insideCarImage;
    public MediaPlayer[] videos;
    public GameObject VideoSphere, ImageSphere;
    public static event Action videoDataLoaded;
    public static event Action<MediaPlayer> videoChanged;

    private List<IEnumerator> runningCoRoutines = new List<IEnumerator>();
    private ApplyToMesh videoToSphere;
    private bool videoInfoGatherd;

    void Start()
    {
        videoToSphere = this.GetComponent<ApplyToMesh>() as ApplyToMesh;
        
        AppManager.appStateChange += OnAppStateChange;

    }

    void OnAppStateChange(AppStates state)
    {
        CancelAllCoRoutines();

        if(state == AppStates.InPlaneStart)
        {
            StartNewCoRoutine(0, DisplayImageOrVideo, true);
            StartNewCoRoutine(0, SetVideo, VideosId.first);
        }
        else if(state == AppStates.ReturnInplaneStart)
        {
            SetVideo(VideosId.first);
        }
        else if(state == AppStates.Falling)
        {
            SetVideo(VideosId.second);
        }
        else if(state == AppStates.OutsideCar360)
        {
            StartNewCoRoutine(PublicAppData.SetVideoDelay, DisplayImageOrVideo, true);
            StartNewCoRoutine(PublicAppData.SetVideoDelay, SetVideo, VideosId.third);
        }
        else if(state == AppStates.InsideCar360)
        {
            SetImageSphereImage(insideCarImage);
            StartNewCoRoutine(PublicAppData.SetVideoDelay, DisplayImageOrVideo, false);
        }
    }

    void Update()
    {
        if(!videoInfoGatherd)
        {
            int loadedVideoCount = 0;
            for (int i = 0; i < videos.Length; i++)
            {
                if (videos[i].Info.GetDurationMs() != 0) loadedVideoCount++; 
            }
            if(loadedVideoCount == videos.Length)
            {
                videoInfoGatherd = true;
                if(videoDataLoaded != null) videoDataLoaded();
            }
        }
    }

    void StartNewCoRoutine<T>(float delay, Action<T> fadeaction, T param)
    {
        var newcoroutine = DelayBeforeVideoAction(delay, fadeaction, param);
        runningCoRoutines.Add(newcoroutine);
        StartCoroutine(newcoroutine);
    }

    IEnumerator DelayBeforeVideoAction<T>(float delay, Action<T> fadeaction, T param)
    {
        yield return new WaitForSeconds(delay);
        fadeaction(param);
    }

    void CancelAllCoRoutines()
    {
        foreach (var coroutine in runningCoRoutines)
        {
            StopCoroutine(coroutine);
        }
    }

    void SetVideo(VideosId id)
    {
        videoToSphere.Player = videos[(int)id];
        if(videoChanged != null) videoChanged(videos[(int)id]);
    }

    public void DisplayImageOrVideo(bool showvideo)
    {
        if(showvideo)
        {
            VideoSphere.SetActive(true);
            ImageSphere.SetActive(false);
        }
        else
        {
            VideoSphere.SetActive(false);
            ImageSphere.SetActive(true);
        }
    }

    public void SetImageSphereImage(Texture img)
    {
        ImageSphere.GetComponent<Renderer>().material.SetTexture("_MainTex", img);
    }

    public enum VideosId
    {
        first = 0,
        second = 1,
        third = 2,
        fourth = 3
    }
}
